﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;

namespace ScottLane.SurgeonV2.WebService.Editors
{
    public class ParameterEditor : CollectionEditor
    {
        /// <summary>
        /// Initialises a new instance of the ParameterEditor editor for the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        public ParameterEditor(Type type) : base(type)
        { }

        protected override CollectionForm CreateCollectionForm()
        {
            CollectionForm form = base.CreateCollectionForm();
            form.Text = "Parameter Editor";
            return form;
        }

        protected override Type[] CreateNewItemTypes()
        {
            return new Type[] { typeof(KeyValuePair<string, string>)};
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected override object CreateInstance(Type type)
        {
            return new KeyValuePair<string, string>();
        }
    }
}
