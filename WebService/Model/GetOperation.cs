﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using ScottLane.SurgeonV2.WebService.DataSources;
using Newtonsoft.Json.Linq;

namespace ScottLane.SurgeonV2.WebService.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class GetOperation
    {
        public IList<string> tags { get; set; }
        public string summary { get; set; }
        public string description { get; set; }
        public string operationId { get; set; }
        public IList<object> consumes { get; set; }
        public IList<string> produces { get; set; }
        public bool deprecated { get; set; }

        /// <summary>
        /// http://www.newtonsoft.com/json/help/html/QueryJsonLinq.htm
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static List<GetOperation> GetGetOperations(SwaggerConnection connection)
        {
            ObjectCache cache = MemoryCache.Default;
            string cacheKey = string.Format("GetGetOperations:{0}", connection.ID);
            List<GetOperation> getOperations = (List<GetOperation>)cache[cacheKey];

            if (getOperations == default(List<GetOperation>))
            {
                getOperations = new List<GetOperation>();
                WebClient client = new WebClient();
                JObject swagger = JObject.Parse(client.DownloadString(connection.DocsUrl));
                IEnumerable<JProperty> properties = from operation in swagger["paths"]
                                                    select (JProperty)operation;

                foreach (JProperty property in properties)
                {
                    if (((JProperty)property.First.First).Name.ToLower() == "get")
                    {
                        GetOperation getOperation = new GetOperation()
                        {
                            operationId = property.Name,
                            summary = property.Name
                        };

                        getOperations.Add(getOperation);
                    }
                }

                cache.Set(cacheKey, getOperations, new CacheItemPolicy());
            }

            return getOperations;
        }
    }
}
