﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottLane.SurgeonV2.WebService.Model
{
    public class Parameter
    {
        public string name { get; set; }
        public string intodo { get; set; }
        public string description { get; set; }
        public bool required { get; set; }
    }
}
