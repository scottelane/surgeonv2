﻿using System.Collections.Generic;
using ScottLane.SurgeonV2.WebService.Model;

namespace ScottLane.SurgeonV2.WebService.Converters
{
    interface IGetOperationsProvider
    {
        List<GetOperation> GetGetOperations();
    }
}
