﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using ScottLane.SurgeonV2.WebService.Model;

namespace ScottLane.SurgeonV2.WebService.Converters
{
    public class GetOperationConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (!string.IsNullOrEmpty((string)value))
            {
                IGetOperationsProvider provider = (IGetOperationsProvider)context.Instance;
                List<GetOperation> operations = provider.GetGetOperations();
                //string operationId = Regex.Match((string)value, @"\((\.*?)\)$").Groups[1].Value;
                return operations.FirstOrDefault(operation => operation.operationId == (string)value);
            }

            return null;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value != default(object))
            {
                GetOperation operation = (GetOperation)value;
                return string.Format("{0}", operation.operationId);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            IGetOperationsProvider provider = (IGetOperationsProvider)context.Instance;
            return new StandardValuesCollection(provider.GetGetOperations());
        }
    }
}
