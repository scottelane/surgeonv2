﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using ScottLane.SurgeonV2.Core;
using ScottLane.SurgeonV2.WebService.Converters;
using ScottLane.SurgeonV2.WebService.Editors;
using ScottLane.SurgeonV2.WebService.Model;

namespace ScottLane.SurgeonV2.WebService.DataSources
{
    /// <summary>
    /// Data source that retrieves data from a REST web service.
    /// </summary>
    [DataSource("Web Service", "Retrieves records from a web service.", "ScottLane.SurgeonV2.WebService.Resources.WebServiceDataSource.png", typeof(SwaggerConnection))]
    public class WebServiceDataSource : DataSource, IGetOperationsProvider
    {
        private const PagingType DEFAULT_PAGING_TYPE = PagingType.DoNotPage;

        /// <summary>
        /// Gets or sets the The GET operation to retrieve data from.
        /// </summary>
        [GlobalisedCategory("Web Service"), GlobalisedDisplayName("Operation"), GlobalisedDecription("The GET operation to retrieve data from."), TypeConverter(typeof(GetOperationConverter))]
        public GetOperation Operation { get; set; }

        /// <summary>
        /// Gets or sets the child node to flatted to.
        /// </summary>
        [GlobalisedCategory("Web Service"), GlobalisedDisplayName("Child Node"), GlobalisedDecription("The child node to flatted to.")]
        public string ChildNode { get; set; }

        /// <summary>
        /// Gets or sets whether paging is required for the operation.
        /// </summary>
        [GlobalisedCategory("Paging"), GlobalisedDisplayName("Paging Type"), GlobalisedDecription("Indicates whether paging is used to retrieve results."), DefaultValue(DEFAULT_PAGING_TYPE)]
        public PagingType PagingType { get; set; }

        /// <summary>
        /// Gets or sets the parameter that defines the page number to retrieve data from.
        /// </summary>
        [GlobalisedCategory("Paging"), GlobalisedDisplayName("Page Number Parameter"), GlobalisedDecription("The parameter that defines the page number to retrieve data from.")]
        public string PageNumberParameter { get; set; }

        /// <summary>
        /// Gets or sets the parameter that defines the number of records to retrieve per page.
        /// </summary>
        [GlobalisedCategory("Paging"), GlobalisedDisplayName("Record Limit Parameter"), GlobalisedDecription("The parameter that defines the number of records to retrieve per page.")]
        public string RecordLimitParameter { get; set; }

        /// <summary>
        /// Gets or sets the parameters to include in the request.
        /// </summary>
        [GlobalisedCategory("TODO"), GlobalisedDisplayName("Parameters"), GlobalisedDecription("The parameters to include in the request."), Editor(typeof(ParameterEditor), typeof(UITypeEditor)), Browsable(true)]
        public Dictionary<string, string> Parameters { get; set; }

        /// <summary>
        /// Initialises a new instance of the WebServiceDataSource class with the specified parent connection.
        /// </summary>
        /// <param name="parentConnection">The parent connection.</param>
        public WebServiceDataSource(Connection parentConnection) : base(parentConnection)
        {
            PagingType = DEFAULT_PAGING_TYPE;
            Parameters = new Dictionary<string, string>();
        }

        public override DataColumnCollection GetDataColumns()
        {
            throw new NotImplementedException();
        }

        public override DataTable GetDataTable(CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            return GetDataTable(cancel, progress, int.MaxValue);
        }

        public override ValidationResult Validate()
        {
            ValidationResult result = base.Validate();

            try
            {
                result.AddErrorIf(Operation == default(GetOperation), "The Operation has not been specified");
            }
            catch (Exception ex)
            {
                result.AddErrorIf(true, ex.Message);
            }

            return result;
        }

        public override DataTable GetDataTable(CancellationToken cancel, IProgress<ExecutionProgress> progress, int recordLimit)
        {
            SwaggerConnection connection = (SwaggerConnection)ParentConnection;
            Uri url = new Uri(connection.BaseUrl, Operation.operationId);
            string queryString = "?";

            foreach (KeyValuePair<string, string> parameter in Parameters)
            {
                queryString = string.Concat(queryString, parameter.Key, "=", parameter.Value);
            }

            url = new Uri(url, queryString);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("Authorization", string.Format("{0}{1}", connection.AuthorisationPrefix, new AESEncrypter().Decrypt(connection.APIKey)));
            WebResponse response = request.GetResponse();
            string json = default(string);

            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                json = streamReader.ReadToEnd();
            }

            using (JsonTextReader jsonReader = new JsonTextReader(new StringReader(json)))
            {
                while (jsonReader.Read())
                {
                    if (jsonReader.Value != null)
                    {
                        Console.WriteLine("Token: {0}, Value: {1}", jsonReader.TokenType, jsonReader.Value);
                    }
                    else
                    {
                        Console.WriteLine("Token: {0}", jsonReader.TokenType);
                    }
                }
            }

            return null;
        }

        public override int GetRecordCount(CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            throw new NotImplementedException();
        }

        public List<GetOperation> GetGetOperations()
        {
            List<GetOperation> getOperations = new List<GetOperation>();

            try
            {
                if (ParentConnection != default(Connection))
                {
                    getOperations.AddRange(GetOperation.GetGetOperations((SwaggerConnection)ParentConnection));
                }
            }
            catch { }

            return getOperations;
        }
    }

    /// <summary>
    /// Defines the types of record paging that are supported.
    /// </summary>
    public enum PagingType
    {
        DoNotPage,
        UsePaging,
    }
}
