﻿using System;
using System.ComponentModel;
using System.Net;
using System.Threading;
using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.WebService.DataSources
{
    /// <summary>
    /// 
    /// </summary>
    [Connection("Swagger", "Provides connectivity to REST web services exposed through Swagger", "ScottLane.SurgeonV2.WebService.Resources.SwaggerConnection.png")]
    public class SwaggerConnection : Connection
    {
        private const string DEFAULT_AUTHORISATION_PREFIX = "Key ";

        [GlobalisedCategory("Swagger"), GlobalisedDisplayName("API base URL"), GlobalisedDecription("The API base URL. Eg. http://myapi.com")]
        public Uri BaseUrl { get; set; }

        [GlobalisedCategory("Swagger"), GlobalisedDisplayName("Swagger Docs URL"), GlobalisedDecription("The Swagger JSON URL. This URL is displayed at the top of the Swagger UI page. Eg. http://myapi.com/swagger/docs/v1")]
        public Uri DocsUrl { get; set; }

        [GlobalisedCategory("Swagger"), GlobalisedDisplayName("API Key"), GlobalisedDecription("The API authorisation key."), TypeConverter(typeof(EncryptedStringConverter))]
        public string APIKey { get; set; }

        [GlobalisedCategory("Swagger"), GlobalisedDisplayName("Authorisation Prefix"), GlobalisedDecription("The prefix to include in the authorisation header before the authorisation key. Eg. Key 7c403091-1ecd-4896-a998-09483441cddc"), DefaultValue(DEFAULT_AUTHORISATION_PREFIX)]
        public string AuthorisationPrefix { get; set; }

        public SwaggerConnection(Project parentProject) : base(parentProject)
        {
            AuthorisationPrefix = DEFAULT_AUTHORISATION_PREFIX;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public override ConnectivityResult IsAvailable(CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            ConnectivityResult result = new ConnectivityResult(false);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(BaseUrl);
                request.Method = "HEAD";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                result.IsAvailable = response.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ValidationResult Validate()
        {
            ValidationResult result = new ValidationResult();

            try
            {
                result.AddErrorIf(BaseUrl == default(Uri), "The API base URL has not been specified");
                result.AddErrorIf(DocsUrl == default(Uri), "The Swagger Docs URL has not been specified");
            }
            catch (Exception ex)
            {
                result.AddErrorIf(true, ex.Message);
            }

            return result;
        }
    }
}
