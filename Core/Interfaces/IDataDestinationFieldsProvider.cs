﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Core
{
    public interface IDataDestinationFieldsProvider
    {
        List<Field> GetDataDestinationFields();
    }
}
