﻿using System;

namespace ScottLane.SurgeonV2.Core
{
    public interface IFieldMappingCreator
    {
        FieldMapping CreateFieldMapping(Type type);
    }
}
