﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Core
{
    public interface IDataSourceFieldsProvider
    {
        List<DataTableField> GetDataSourceFields();
    }
}
