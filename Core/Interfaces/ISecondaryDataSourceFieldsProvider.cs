﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Core
{
    public interface ISecondaryDataSourceFieldsProvider
    {
        List<DataTableField> GetSecondaryDataSourceFields();
    }
}
