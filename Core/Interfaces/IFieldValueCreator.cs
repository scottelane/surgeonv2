﻿using System;

namespace ScottLane.SurgeonV2.Core
{
    public interface IFieldValueCreator
    {
        FieldValue CreateFieldValue(Type type);
    }
}
