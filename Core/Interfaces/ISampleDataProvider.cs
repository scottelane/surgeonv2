﻿using System;
using System.Data;
using System.Threading;

namespace ScottLane.SurgeonV2.Core
{
    public interface ISampleDataProvider
    {
        DataTable GetSampleData(CancellationToken cancel, IProgress<ExecutionProgress> progress, int recordLimit);
    }
}
