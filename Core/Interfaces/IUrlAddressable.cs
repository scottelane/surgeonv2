﻿using System;

namespace ScottLane.SurgeonV2.Core
{
    public interface IUrlAddressable
    {
        Uri Url { get; }
    }
}
