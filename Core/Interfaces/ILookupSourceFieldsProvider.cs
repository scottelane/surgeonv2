﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Core
{
    public interface ILookupSourceFieldsProvider
    {
        List<DataTableField> GetLookupSourceFields();
    }
}
