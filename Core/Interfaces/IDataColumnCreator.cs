﻿using System;
using System.Data;

namespace ScottLane.SurgeonV2.Core
{
    public interface IDataColumnCreator
    {
        DataColumn CreateDataColumn(Type type);
    }
}
