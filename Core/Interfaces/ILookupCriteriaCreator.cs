﻿using System;

namespace ScottLane.SurgeonV2.Core
{
    public interface ILookupCriteriaCreator
    {
        LookupCriteria CreateLookupCriteria(Type type);
    }
}
