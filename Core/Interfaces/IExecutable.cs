﻿using System;
using System.Threading;

namespace ScottLane.SurgeonV2.Core
{
    /// <summary>
    /// Interface for all objects that can be executed by the application.
    /// </summary>
    public interface IExecutable
    {
        /// <summary>
        /// Gets the item name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the total number of items that will be executed.
        /// </summary>
        int OperationCount { get; }

        /// <summary>
        /// Executes the item and any child items.
        /// </summary>
        /// <param name="cancel"></param>
        /// <param name="progress"></param>
        void Execute(CancellationToken cancel, IProgress<ExecutionProgress> progress);

        /// <summary>
        /// Validates the item before execution.
        /// </summary>
        /// <returns>The validation result.</returns>
        ValidationResult Validate();
    }
}