﻿using System;
using System.Threading;

namespace ScottLane.SurgeonV2.Core
{
    public interface IRecordCountProvider
    {
        int GetRecordCount(CancellationToken cancel, IProgress<ExecutionProgress> progress);
    }
}
