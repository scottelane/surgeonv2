﻿using System.ComponentModel;

namespace ScottLane.SurgeonV2.Core
{
    public interface IDataSourcesProvider
    {
        BindingList<IDataSource> GetDataSources();
    }
}
