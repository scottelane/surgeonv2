﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace ScottLane.SurgeonV2.Core
{
    public interface ICustomMenuItemProvider
    {
        List<CustomMenuItem> GetCustomMenuItems();
    }
}
