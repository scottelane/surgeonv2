﻿namespace ScottLane.SurgeonV2.Core
{
    /// <summary>
    /// Provides reporting of execution progress.
    /// </summary>
    public class ExecutionProgress
    {
        /// <summary>
        /// Gets or sets the type of progress that is being reported.
        /// </summary>
        public ProgressType ProgressType { get; set; }

        /// <summary>
        /// Gets or sets the execution stage.
        /// </summary>
        public ExecutionStage ExecutionStage { get; set; }

        /// <summary>
        /// Gets or sets the total number of items to execute.
        /// </summary>
        public int ItemCount { get; set; }

        /// <summary>
        /// Gets or sets the index of the currently executing item.
        /// </summary>
        public int ItemIndex { get; set; }

        /// <summary>
        /// Gets or sets the index of the currently executing operation.
        /// </summary>
        public int OperationIndex { get; set; }

        /// <summary>
        /// Gets or sets the type of notification.
        /// </summary>
        public NotificationType NotificationType { get; set; }

        /// <summary>
        /// Gets or sets the notification message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Initialises a new instance of the ExecutionProgress class for reporting execution progress messages.
        /// </summary>
        /// <param name="notificationType">The notification type.</param>
        /// <param name="message">The progress message.</param>
        public ExecutionProgress(NotificationType notificationType, string message)
        {
            NotificationType = notificationType;
            Message = message;
            ProgressType = ProgressType.Notification;
        }

        /// <summary>
        /// Initialises a new instance of the ExecutionProgress class for quantitative execution progress. 
        /// </summary>
        /// <param name="executionStage">The operation type.</param>
        /// <param name="itemIndex">The index of the currently executing item.</param>
        /// <param name="itemCount">The total number of items to execute.</param>
        public ExecutionProgress(ExecutionStage executionStage, int itemIndex, int itemCount)
        {
            ExecutionStage = executionStage;
            ItemIndex = itemIndex;
            ItemCount = itemCount;
            ProgressType = ProgressType.ItemProgress;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationIndex"></param>
        public ExecutionProgress(int operationIndex)
        {
            OperationIndex = operationIndex;
            ProgressType = ProgressType.OperationProgress;
        }
    }

    /// <summary>
    /// Defines the types of progress that can be reported.
    /// </summary>
    public enum ProgressType
    {
        Notification,
        ItemProgress,
        OperationProgress
    }

    /// <summary>
    /// Defines the execution stages that can be reported.
    /// </summary>
    public enum ExecutionStage
    {
        Extract,
        Transform,
        Load
    }
}
