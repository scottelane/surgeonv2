﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ScottLane.SurgeonV2.Core
{
    public class CoreUtility
    {
        public static List<Type> GetInterfaceImplementorsWithAttribute(Type interfaceType, Type attributeType)
        {
            List<Type> interfaceImplementors = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(t => t.IsClass
                    && !t.IsAbstract
                    && interfaceType.IsAssignableFrom(t)
                    && Attribute.GetCustomAttribute(t, attributeType) != default(Attribute)).ToList();

            return interfaceImplementors;
        }

        public static void SetBrowsable(object item, string propertyName, bool visible)
        {
            PropertyDescriptor descriptor = TypeDescriptor.GetProperties(item.GetType())[propertyName];
            BrowsableAttribute attribute = (BrowsableAttribute)descriptor.Attributes[typeof(BrowsableAttribute)];
            FieldInfo browsable = attribute.GetType().GetField("browsable", BindingFlags.NonPublic | BindingFlags.Instance);
            browsable.SetValue(attribute, visible);
        }

        public static readonly string FieldMatchPattern = @"\(((?:[^()]|(?<open>\()|(?<-open>\)))+)\)$";
    }
}
