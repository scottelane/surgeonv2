﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace ScottLane.SurgeonV2.Core
{
    /// <summary>
    /// Provides a basic in-memory stage cache for connection-related objects.
    /// </summary>
    public class ConnectionCache
    {
        private const string SEPARATOR = ":";

        /// <summary>
        /// Gets the Connection associated with the cache.
        /// </summary>
        public IConnection Connection { get; private set; }

        /// <summary>
        /// Initialises a new instance of the ConnectionCache class for the specified Connection.
        /// </summary>
        /// <param name="connection">The Connection.</param>
        public ConnectionCache(IConnection connection)
        {
            Connection = connection;
        }

        /// <summary>
        /// Gets or sets the cached object with the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The cached object.</returns>
        public object this[string key]
        {
            get
            {
                return MemoryCache.Default[string.Concat(Connection.ID.ToString(), SEPARATOR, key)];
            }
            set
            {
                MemoryCache.Default[string.Concat(Connection.ID.ToString(), SEPARATOR, key)] = value;
            }
        }

        /// <summary>
        /// Returns the number of items in the cache.
        /// </summary>
        /// <returns>The item count.</returns>
        public int Count
        {
            get
            {
                int itemCount = 0;
                ObjectCache cache = MemoryCache.Default;
                List<string> keys = cache.Select(key => key.Key).ToList();

                foreach (string key in keys)
                {
                    itemCount++;
                }

                return itemCount;
            }
        }

        /// <summary>
        /// Clears all cached objects for the connection.
        /// </summary>
        public void Clear()
        {
            ObjectCache cache = MemoryCache.Default;
            List<string> keys = cache.Select(key => key.Key).ToList();

            foreach (string key in keys)
            {
                if (key.StartsWith(string.Concat(Connection.ID.ToString(), SEPARATOR)))
                {
                    cache.Remove(key);
                }
            }
        }
    }
}
