﻿namespace ScottLane.SurgeonV2.Core
{
    /// <summary>
    /// Defines the types of Notifications that can be raised.
    /// </summary>
    public enum NotificationType
    {
        Information,
        Warning,
        Error
    }
}
