﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Newtonsoft.Json;
using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.Dynamics365
{
    /// <summary>
    /// Assigns Dynamics 365 records to a user or team.
    /// </summary>
    [Operation(typeof(Dynamics365AssignOperation), "ScottLane.SurgeonV2.Dynamics365.Resources.Dynamics365AssignOperation.png")]
    public class Dynamics365AssignOperation : Dynamics365RecordOperation, IDynamics365UserOrTeamOwnedEntitiesProvider, IDynamics365TeamsProvider, IDynamics365UsersProvider, INotifyPropertyChanged
    {
        private const OwnerType DEFAULT_OWNER_TYPE = OwnerType.User;

        #region Properties

        /// <summary>
        /// Gets or sets the source of the target record identifier.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(TargetSource)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(TargetSource)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(TargetSource)), DefaultValue(DEFAULT_TARGET_SOURCE)]
        public override TargetSource TargetSource
        {
            get { return base.TargetSource; }
            set { base.TargetSource = value; }
        }

        /// <summary>
        /// Gets or sets the identifier of the record targeted by the operation.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(Target)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(Target)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(Target)), TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
        public override FieldValue Target
        {
            get { return base.Target; }
            set { base.Target = value; }
        }

        /// <summary>
        /// Gets or sets the data source to read records from.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(DataSource)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(DataSource)), GlobalisedDecription(typeof(Dynamics365AssignOperation), nameof(DataSource)), TypeConverter(typeof(DataSourceConverter))]
        public override IDataSource DataSource
        {
            get { return base.DataSource; }
            set { base.DataSource = value; }
        }

        /// <summary>
        /// Gets or sets a connection to the Dynamics 365 organisation to assign records in.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(Connection)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(Connection)), GlobalisedDecription(typeof(Dynamics365AssignOperation), nameof(Connection)), TypeConverter(typeof(ConnectionConverter)), JsonProperty(Order = 1)]
        public override Dynamics365Connection Connection
        {
            get { return base.Connection; }
            set { base.Connection = value; }
        }

        /// <summary>
        /// Gets or sets the Dynamics365Entity containing records to assign.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(Entity)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(Entity)), GlobalisedDecription(typeof(Dynamics365AssignOperation), nameof(Entity)), TypeConverter(typeof(Dynamics365UserOrTeamOwnedEntityConverter)), JsonProperty(Order = 2)]
        public override Dynamics365Entity Entity
        {
            get { return base.Entity; }
            set { base.Entity = value; }
        }

        private OwnerType ownerType = DEFAULT_OWNER_TYPE;

        /// <summary>
        /// Gets or sets the type of owner to assign records to.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(OwnerType)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(OwnerType)), GlobalisedDecription(typeof(Dynamics365AssignOperation), nameof(OwnerType)), DefaultValue(DEFAULT_OWNER_TYPE)]
        public OwnerType OwnerType
        {
            get { return ownerType; }
            set
            {
                if (ownerType != value)
                {
                    ownerType = value;
                    CoreUtility.SetBrowsable(this, nameof(Team), ownerType == OwnerType.Team);
                    CoreUtility.SetBrowsable(this, nameof(User), ownerType == OwnerType.User);
                    OnPropertyChanged(nameof(OwnerType));
                    RefreshName();
                }
            }
        }

        private Dynamics365Team team;

        /// <summary>
        /// Gets or sets the Dynamics365Team to assign as the record owner.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(Team)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(Team)), GlobalisedDecription(typeof(Dynamics365AssignOperation), nameof(Team)), TypeConverter(typeof(Dynamics365TeamConverter)), Browsable(false), JsonProperty(Order = 3)]
        public Dynamics365Team Team
        {
            get { return team; }
            set
            {
                if (team != value)
                {
                    team = value;
                    OnPropertyChanged(nameof(Team));
                    RefreshName();
                }
            }
        }

        private Dynamics365User user;

        /// <summary>
        /// Gets or sets the Dynamics365User to assign as the record owner.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365AssignOperation), nameof(User)), GlobalisedDisplayName(typeof(Dynamics365AssignOperation), nameof(User)), GlobalisedDecription(typeof(Dynamics365AssignOperation), nameof(User)), TypeConverter(typeof(Dynamics365UserConverter)), Browsable(true), JsonProperty(Order = 3)]
        public Dynamics365User User
        {
            get { return user; }
            set
            {
                if (user != value)
                {
                    user = value;
                    OnPropertyChanged(nameof(User));
                    RefreshName();
                }
            }
        }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Dynamics365AssignOperation class with the specified parent batch.
        /// </summary>
        /// <param name="parentBatch">The parent batch.</param>
        public Dynamics365AssignOperation(Batch parentBatch) : base(parentBatch)
        { }

        /// <summary>
        /// Generates a friendly name for the operation.
        /// </summary>
        /// <returns>The friendly name.</returns>
        protected override string GenerateFriendlyName()
        {
            return string.Format(Properties.Resources.Dynamics365AssignOperationFriendlyName,
                Entity?.PluralName ?? Properties.Resources.Dynamics365AssignOperationFriendlyNameEntity,
                OwnerType == OwnerType.Team ? Team?.Name ?? Properties.Resources.Dynamics365AssignOperationFriendlyNameTeam : OwnerType == OwnerType.User ? User?.Name ?? Properties.Resources.Dynamics365AssignOperationFriendlyNameUser : Properties.Resources.Dynamics365AssignOperationFriendlyNameUnknown,
                DataSource?.Name ?? Properties.Resources.Dynamics365AssignOperationFriendlyNameDataSource);
        }

        /// <summary>
        /// Validates the operation.
        /// </summary>
        /// <returns>The ValidationResult.</returns>
        public override Core.ValidationResult Validate()
        {
            Core.ValidationResult result = base.Validate();
            result.AddErrorIf(OwnerType == OwnerType.User && User == default(Dynamics365User), Properties.Resources.Dynamics365AssignOperationValidateUser, nameof(User));
            result.AddErrorIf(OwnerType == OwnerType.Team && Team == default(Dynamics365Team), Properties.Resources.Dynamics365AssignOperationValidateTeam, nameof(Team));

            return result;
        }

        /// <summary>
        /// Create a list of OrganizationRequest objects for the specified data row.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>A list of OrganizationRequest objects.</returns>
        protected override List<OrganizationRequest> CreateOrganisationRequests(DataRow row, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            List<OrganizationRequest> requests = new List<OrganizationRequest>();

            if (Connection.IsVersionOrAbove(CrmVersion.Crm2016))
            {
                UpdateRequest request = new UpdateRequest()
                {
                    Target = GetTargetEntity(row, cancel, progress)
                };

                request.Target.Attributes["ownerid"] = User != null ? User.ToEntityReference() : Team.ToEntityReference();
                requests.Add(request);
            }
            else
            {
                requests.Add(new AssignRequest()
                {
                    Assignee = User != null ? User.ToEntityReference() : Team.ToEntityReference(),
                    Target = GetTargetEntity(row, cancel, progress).ToEntityReference()
                });
            }

            return requests;
        }

        /// <summary>
        /// Gets a description of the specified request. 
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The request description.</returns>
        protected override string GetRequestDescription(OrganizationRequest request)
        {
            string requestDescription = string.Empty;

            if (request is UpdateRequest updateRequest)
            {
                requestDescription = string.Format(Properties.Resources.Dynamics365AssignOperationRequestDescription, Entity.DisplayName, updateRequest.Target.Id, User != null ? User.Name : Team.Name);
            }
            else if (request is AssignRequest assignRequest)
            {
                requestDescription = string.Format(Properties.Resources.Dynamics365AssignOperationRequestDescription, Entity.DisplayName, assignRequest.Target.Id, User != null ? User.Name : Team.Name);
            }

            return requestDescription;
        }

        #region Interface methods

        /// <summary>
        /// Gets a list of entities that are owned by a user or team.
        /// </summary>
        /// <returns>A list of Dynamics365Entity objects.</returns>
        public List<Dynamics365Entity> GetUserOrTeamOwnedEntities()
        {
            List<Dynamics365Entity> entities = new List<Dynamics365Entity>();

            try
            {
                entities.AddRange(Dynamics365Entity.GetEntities(Connection).Where(entity => entity.IsUserTeamOwned).ToList());
            }
            catch { }

            return entities;
        }

        /// <summary>
        /// Gets a list of Dynamics 365 teams.
        /// </summary>
        /// <returns>A list of Dynamics365Team objects.</returns>
        public List<Dynamics365Team> GetTeams()
        {
            List<Dynamics365Team> teams = new List<Dynamics365Team>();

            try
            {
                teams.AddRange(Dynamics365Team.GetTeams(Connection).ToList());
            }
            catch { }

            return teams;
        }

        /// <summary>
        /// Gets a list of Dynamics 365 users.
        /// </summary>
        /// <returns>A list of DynamcisCrmUser objects.</returns>
        public List<Dynamics365User> GetUsers()
        {
            List<Dynamics365User> users = new List<Dynamics365User>();

            try
            {
                users.AddRange(Dynamics365User.GetUsers(Connection).ToList());
            }
            catch { }

            return users;
        }

        #endregion
    }

    /// <summary>
    /// Defines the types of entity ownership.
    /// </summary>
    public enum OwnerType
    {
        Team,
        User
    }
}