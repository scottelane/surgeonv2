﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.Dynamics365
{
    /// <summary>
    /// Runs an on-demand Dynamics 365 workflow against a record.
    /// </summary>
    [Operation(typeof(Dynamics365RunWorkflowOperation), "ScottLane.SurgeonV2.Dynamics365.Resources.Dynamics365RunWorkflowOperation.png")]
    public class Dynamics365RunWorkflowOperation : Dynamics365RecordOperation, IDynamics365WorkflowProvider
    {
        private Dynamics365Workflow workflow;

        [GlobalisedCategory(typeof(Dynamics365RunWorkflowOperation), nameof(Workflow)), GlobalisedDisplayName(typeof(Dynamics365RunWorkflowOperation), nameof(Workflow)), GlobalisedDecription(typeof(Dynamics365RunWorkflowOperation), nameof(Workflow)), TypeConverter(typeof(Dynamics365WorkflowConverter))]
        public Dynamics365Workflow Workflow
        {
            get { return workflow; }
            set
            {
                if (workflow != value)
                {
                    workflow = value;
                    OnPropertyChanged(nameof(Workflow));
                    RefreshName();
                }
            }
        }

        public Dynamics365RunWorkflowOperation(Batch parentBatch) : base(parentBatch)
        { }

        protected override List<OrganizationRequest> CreateOrganisationRequests(DataRow row, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            List<OrganizationRequest> requests = new List<OrganizationRequest>();

            requests.Add(new ExecuteWorkflowRequest()
            {
                WorkflowId = workflow.ID,
                EntityId = GetTargetEntity(row, cancel, progress).ToEntityReference().Id
            });

            return requests;
        }

        public override Core.ValidationResult Validate()
        {
            Core.ValidationResult result = base.Validate();
            result.AddErrorIf(Workflow == default(Dynamics365Workflow), Properties.Resources.Dynamics365RunWorkflowOperationValidateWorkflow, nameof(Dynamics365Workflow));
            return result;
        }

        /// <summary>
        /// Generates a friendly name for the operation.
        /// </summary>
        /// <returns>The friendly name.</returns>
        protected override string GenerateFriendlyName()
        {
            return string.Format("Run {0} for records in {1}", Workflow?.Name ?? "<Workflow>", DataSource?.Name ?? "<Data Source>");
        }

        protected override string GetRequestDescription(OrganizationRequest request)
        {
            ExecuteWorkflowRequest workflowRequest = (ExecuteWorkflowRequest)request;
            return string.Format("Running {0} for {1}", Workflow.Name, workflowRequest.EntityId);
        }

        public List<Dynamics365Workflow> GetWorkflows()
        {
            List<Dynamics365Workflow> workflows = new List<Dynamics365Workflow>();

            try
            {
                workflows.AddRange(Dynamics365Workflow.GetWorkflows(Connection, Entity.LogicalName));
            }
            catch { }

            return workflows;
        }
    }
}
