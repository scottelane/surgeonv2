﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.Dynamics365
{
    [Operation(typeof(Dynamics365SetStateOperation), "ScottLane.SurgeonV2.Dynamics365.Resources.Dynamics365SetStateOperation.png")]
    public class Dynamics365SetStateOperation : Dynamics365RecordOperation, IDynamics365StatesProvider, IDynamics365StatusesProvider
    {
        public const string CASE_ENTITY_NAME = "incident";
        public const string CASE_INCIDENT_ID = "incidentid";
        public const string CASE_RESOLUTION_ENTITY_NAME = "incidentresolution";
        public const string QUOTE_ENTITY_NAME = "quote";
        public const string QUOTE_ID = "quoteid";
        public const string QUOTE_CLOSE_ENTITY_NAME = "quoteclose";
        public const string OPPORTUNITY_ENTITY_NAME = "opportunity";
        public const string OPPORTUNITY_CLOSE_ENTITY_NAME = "opportunityclose";
        public const string OPPORTUNITY_ID = "opportunityid";

        #region Properties

        private Dynamics365State state;

        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(State)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(State)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(State)), TypeConverter(typeof(Dynamics365StateConverter))]
        public Dynamics365State State
        {
            get { return state; }
            set
            {
                if (state != value)
                {
                    state = value;
                    OnPropertyChanged(nameof(State));
                    RefreshName();
                }
            }
        }

        private Dynamics365Status status;

        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(Status)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(Status)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(Status)), TypeConverter(typeof(Dynamics365StatusConverter))]
        public Dynamics365Status Status
        {
            get { return status; }
            set
            {
                if (status != value)
                {
                    status = value;
                    OnPropertyChanged(nameof(Status));
                    RefreshName();
                    UpdateFieldBrowsableStatus();
                }
            }
        }

        private string resolution;

        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(Resolution)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(Resolution)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(Resolution)), Browsable(false)]
        public string Resolution
        {
            get { return resolution; }
            set
            {
                if (resolution != value)
                {
                    resolution = value;
                    OnPropertyChanged(nameof(Resolution));
                    RefreshName();
                }
            }
        }

        private string remarks;

        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(Remarks)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(Remarks)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(Remarks)), Browsable(false)]
        public string Remarks
        {
            get { return remarks; }
            set
            {
                if (remarks != value)
                {
                    remarks = value;
                    OnPropertyChanged(nameof(Remarks));
                    RefreshName();
                }
            }
        }
        private void UpdateFieldBrowsableStatus()
        {
            if (Entity != default(Dynamics365Entity) && Entity.LogicalName == CASE_ENTITY_NAME)
            {
                CoreUtility.SetBrowsable(this, nameof(Resolution), true);
                CoreUtility.SetBrowsable(this, nameof(Remarks), true);
            }
            else
            {
                CoreUtility.SetBrowsable(this, nameof(Resolution), false);
                CoreUtility.SetBrowsable(this, nameof(Remarks), false);
            }
        }

        /// <summary>
        /// Gets or sets the source of the target record identifier.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(TargetSource)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(TargetSource)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(TargetSource)), DefaultValue(DEFAULT_TARGET_SOURCE)]
        public override TargetSource TargetSource
        {
            get { return base.TargetSource; }
            set { base.TargetSource = value; }
        }

        /// <summary>
        /// Gets or sets the identifier of the record targeted by the operation.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(Target)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(Target)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(Target)), TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
        public override FieldValue Target
        {
            get { return base.Target; }
            set { base.Target = value; }
        }

        /// <summary>
        /// Gets or sets the data source to read records from.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(DataSource)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(DataSource)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(DataSource)), TypeConverter(typeof(DataSourceConverter))]
        public override IDataSource DataSource
        {
            get { return base.DataSource; }
            set { base.DataSource = value; }
        }

        /// <summary>
        /// Gets or sets a connection to the Dynamics 365 organisation to assign records in.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(Connection)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(Connection)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(Connection)), TypeConverter(typeof(ConnectionConverter)), JsonProperty(Order = 1)]
        public override Dynamics365Connection Connection
        {
            get { return base.Connection; }
            set { base.Connection = value; }
        }

        /// <summary>
        /// Gets or sets the Dynamics365Entity containing records to assign.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365SetStateOperation), nameof(Entity)), GlobalisedDisplayName(typeof(Dynamics365SetStateOperation), nameof(Entity)), GlobalisedDecription(typeof(Dynamics365SetStateOperation), nameof(Entity)), TypeConverter(typeof(Dynamics365EntityConverter)), JsonProperty(Order = 2)]
        public override Dynamics365Entity Entity
        {
            get { return base.Entity; }
            set
            {
                if (entity != value)
                {
                    entity = value;
                    OnPropertyChanged(nameof(Entity));
                    RefreshName();
                    UpdateFieldBrowsableStatus();
                }
            }
        }

        #endregion

        public Dynamics365SetStateOperation(Batch parentBatch) : base(parentBatch)
        { }

        public override Core.ValidationResult Validate()
        {
            Core.ValidationResult result = base.Validate();
            result.AddErrorIf(State == default(Dynamics365State), Properties.Resources.Dynamics365SetStateOperationValidateState, nameof(State));
            result.AddErrorIf(Status == default(Dynamics365Status), Properties.Resources.Dynamics365SetStateOperationValidateStatus, nameof(Status));
            return result;
        }

        protected override string GenerateFriendlyName()
        {
            return string.Format(Properties.Resources.Dynamics365SetStateOperationFriendlyName, Entity?.PluralName ?? Properties.Resources.Dynamics365SetStateOperationFriendlyNameEntity, State?.Name ?? Properties.Resources.Dynamics365SetStateOperationFriendlyNameState, Status?.Name ?? Properties.Resources.Dynamics365SetStateOperationFriendlyNameStatus);
        }

        protected override List<OrganizationRequest> CreateOrganisationRequests(DataRow row, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            List<OrganizationRequest> requests = new List<OrganizationRequest>();

            // todo - replace with updaterequest for crm2015 update +
            if (Entity.LogicalName == CASE_ENTITY_NAME && State.Code == StateCode.CaseResolved)
            {
                Entity incidentResolution = new Entity(CASE_RESOLUTION_ENTITY_NAME);
                incidentResolution[CASE_INCIDENT_ID] = GetTargetEntity(row, cancel, progress).ToEntityReference();
                incidentResolution["statecode"] = new OptionSetValue(State.Code);
                incidentResolution["subject"] = remarks;
                incidentResolution["description"] = resolution;

                requests.Add(new CloseIncidentRequest()
                {
                    IncidentResolution = incidentResolution,
                    Status = new OptionSetValue(Status.Code)
                });
            }
            else if (Entity.LogicalName == QUOTE_ENTITY_NAME && State.Code == StateCode.QuoteDraft)
            {
                requests.Add(new ReviseQuoteRequest()
                {
                    QuoteId = GetTargetEntity(row, cancel, progress).Id,
                    ColumnSet = new ColumnSet(true)
                });
            }
            else if (Entity.LogicalName == QUOTE_ENTITY_NAME && State.Code == StateCode.QuoteWon)
            {
                Entity quoteClose = new Entity(QUOTE_CLOSE_ENTITY_NAME);
                quoteClose[QUOTE_ID] = GetTargetEntity(row, cancel, progress).ToEntityReference();
                quoteClose["statecode"] = new OptionSetValue(State.Code);

                requests.Add(new WinQuoteRequest()
                {
                    QuoteClose = quoteClose,
                    Status = new OptionSetValue(Status.Code),
                });
            }
            else if (Entity.LogicalName == QUOTE_ENTITY_NAME && State.Code == StateCode.QuoteClosed)
            {
                Entity quoteClose = new Entity(QUOTE_CLOSE_ENTITY_NAME);
                quoteClose[QUOTE_ID] = GetTargetEntity(row, cancel, progress).ToEntityReference();
                quoteClose["statecode"] = new OptionSetValue(State.Code);

                requests.Add(new CloseQuoteRequest()
                {
                    QuoteClose = quoteClose,
                    Status = new OptionSetValue(Status.Code)
                });
            }
            else if (Entity.LogicalName == OPPORTUNITY_ENTITY_NAME && State.Code == StateCode.OpportunityWon)
            {
                Entity opportunityClose = new Entity(OPPORTUNITY_CLOSE_ENTITY_NAME);
                opportunityClose[OPPORTUNITY_ID] = GetTargetEntity(row, cancel, progress).ToEntityReference();
                //opportunityClose["subject"] = "Won";
                //opportunityClose["description"] = "Won";
                //opportunityClose["actualend"] = DateTime.Now;
                //opportunityClose["actualrevenue"] = new Money(100);

                requests.Add(new WinOpportunityRequest
                {
                    OpportunityClose = opportunityClose,
                    Status = new OptionSetValue(Status.Code)
                });
            }
            else if (Entity.LogicalName == OPPORTUNITY_ENTITY_NAME && State.Code == StateCode.OpportunityLost)
            {
                Entity opportunityClose = new Entity(OPPORTUNITY_CLOSE_ENTITY_NAME);
                opportunityClose[OPPORTUNITY_ID] = GetTargetEntity(row, cancel, progress).ToEntityReference();
                //opportunityClose["subject"] = "Won";
                //opportunityClose["description"] = "Won";
                //opportunityClose["actualend"] = DateTime.Now;
                //opportunityClose["actualrevenue"] = new Money(100);
                //todo - reason for loss

                requests.Add(new LoseOpportunityRequest
                {
                    OpportunityClose = opportunityClose,
                    Status = new OptionSetValue(Status.Code)
                });
            }
            else
            {
                requests.Add(new SetStateRequest()
                {
                    EntityMoniker = GetTargetEntity(row, cancel, progress).ToEntityReference(),
                    State = new OptionSetValue(State.Code),
                    Status = new OptionSetValue(Status.Code)
                });
            }

            return requests;
        }

        protected override string GetRequestDescription(OrganizationRequest request)
        {
            Guid id = Guid.Empty;

            if (request is SetStateRequest)
            {
                id = ((SetStateRequest)request).EntityMoniker.Id;
            }
            else if (request is CloseIncidentRequest)
            {
                id = ((EntityReference)((CloseIncidentRequest)request).IncidentResolution[CASE_INCIDENT_ID]).Id;
            }
            else if (request is ReviseQuoteRequest)
            {
                id = ((ReviseQuoteRequest)request).QuoteId;
            }
            else if (request is WinQuoteRequest)
            {
                id = ((EntityReference)((WinQuoteRequest)request).QuoteClose[QUOTE_ID]).Id;
            }
            else if (request is CloseQuoteRequest)
            {
                id = ((EntityReference)((CloseQuoteRequest)request).QuoteClose[QUOTE_ID]).Id;
            }
            else if (request is WinOpportunityRequest)
            {
                id = ((EntityReference)((WinOpportunityRequest)request).OpportunityClose[OPPORTUNITY_ID]).Id;
            }
            else if (request is LoseOpportunityRequest)
            {
                id = ((EntityReference)((LoseOpportunityRequest)request).OpportunityClose[OPPORTUNITY_ID]).Id;
            }

            return string.Format(Properties.Resources.Dynamics365SetStateOperationRequestDescription, Entity.DisplayName, id, State.Name, Status.Name);
        }

        public List<Dynamics365State> GetStates()
        {
            List<Dynamics365State> states = new List<Dynamics365State>();

            try
            {
                states.AddRange(Dynamics365State.GetStates(Entity, Connection));
            }
            catch
            { }

            return states;
        }

        public List<Dynamics365Status> GetStatuses()
        {
            List<Dynamics365Status> statuses = new List<Dynamics365Status>();

            try
            {
                statuses.AddRange(State.Statuses);
            }
            catch
            { }

            return statuses;
        }
    }

    public class StateCode
    {
        public static readonly int CaseResolved = 1;
        public static readonly int QuoteDraft = 0;
        public static readonly int QuoteWon = 2;
        public static readonly int QuoteClosed = 3;
        public static readonly int OpportunityWon = 1;
        public static readonly int OpportunityLost = 2;
    }
}