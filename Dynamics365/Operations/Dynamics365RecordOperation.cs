﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using ScottLane.SurgeonV2.Core;
using Newtonsoft.Json;

namespace ScottLane.SurgeonV2.Dynamics365
{
    /// <summary>
    /// Base class for Dynamics 365 operations that perform an action against records in an entity.
    /// </summary>
    public abstract class Dynamics365RecordOperation : Dynamics365Operation, IDataDestinationFieldsProvider, ILookupCriteriaCreator, IDataSourceFieldsProvider, IDynamics365EntityFieldsProvider, IExecutable
    {
        protected const TargetSource DEFAULT_TARGET_SOURCE = TargetSource.DataSourceValue;
        protected const ProcessingMode DEFAULT_PROCESSING_MODE = ProcessingMode.Individual;
        protected const int DEFAULT_BATCH_SIZE = 100;
        protected const bool DEFAULT_CONTINUE_ON_ERROR = false;

        #region Properties

        protected Dynamics365Entity entity;

        /// <summary>
        /// Gets or sets the entity targeted by the operation.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(Entity)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(Entity)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(Entity)), TypeConverter(typeof(Dynamics365EntityConverter)), Browsable(true)]
        public virtual Dynamics365Entity Entity
        {
            get { return entity; }
            set
            {
                if (entity != value)
                {
                    entity = value;
                    OnPropertyChanged(nameof(Entity));
                    RefreshName();
                }
            }
        }

        protected TargetSource targetSource;

        /// <summary>
        /// Gets or sets the source of the target record identifier.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(TargetSource)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(TargetSource)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(TargetSource)), DefaultValue(DEFAULT_TARGET_SOURCE)]
        public virtual TargetSource TargetSource
        {
            get { return targetSource; }
            set
            {
                SetTargetSource(value);
            }
        }

        /// <summary>
        /// Sets the TargetSource value and makes the appropriate properties browsable.
        /// </summary>
        /// <param name="value"></param>
        protected virtual void SetTargetSource(TargetSource value)
        {
            if (targetSource != value)
            {
                targetSource = value;

                if (targetSource == TargetSource.DataSourceValue)
                {
                    Target = new Dynamics365DataSourceValue(this);
                    CoreUtility.SetBrowsable(this, nameof(Target), true);
                }
                else if (targetSource == TargetSource.LookupValue)
                {
                    Target = new Dynamics365LookupValue(this);
                    CoreUtility.SetBrowsable(this, nameof(Target), true);
                }
                else if (targetSource == TargetSource.UserProvidedValue)
                {
                    Target = new Dynamics365UserProvidedValue(this);
                    CoreUtility.SetBrowsable(this, nameof(Target), true);
                }
                else
                {
                    Target = default(FieldValue);
                    CoreUtility.SetBrowsable(this, nameof(Target), false);
                }

                OnPropertyChanged(nameof(TargetSource));
                RefreshName();
                // todo - events if properties within target source change
            }
        }

        protected FieldValue target;

        /// <summary>
        /// Gets or sets the identifier of the record targeted by the operation.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(Target)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(Target)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(Target)), TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
        public virtual FieldValue Target
        {
            get { return target; }
            set
            {
                if (target != value)
                {
                    target = value;
                    OnPropertyChanged(nameof(Target));
                    RefreshName();
                }
            }
        }

        protected IDataSource dataSource;

        /// <summary>
        /// Gets or sets the data source to read records from.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(DataSource)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(DataSource)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(DataSource)), TypeConverter(typeof(DataSourceConverter)), JsonProperty(Order = 1)]
        public virtual IDataSource DataSource
        {
            get { return dataSource; }
            set
            {
                if (dataSource != value)
                {
                    dataSource = value;
                    OnPropertyChanged(nameof(DataSource));
                    RefreshName();
                }
            }
        }

        protected int batchSize = DEFAULT_BATCH_SIZE;

        /// <summary>
        /// Gets or sets the number of records that will be processed per batch if the Batch processing mode is used.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(BatchSize)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(BatchSize)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(BatchSize)), DefaultValue(DEFAULT_BATCH_SIZE), Browsable(false)]
        public virtual int BatchSize
        {
            get { return batchSize; }
            set
            {
                if (batchSize != value)
                {
                    batchSize = value;
                    OnPropertyChanged(nameof(batchSize));
                    RefreshName();
                }
            }
        }

        protected bool continueOnError = DEFAULT_CONTINUE_ON_ERROR;

        /// <summary>
        /// Gets or sets a value that determines whether subsequent operations are processed if an error occurs.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(ContinuteOnError)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(ContinuteOnError)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(ContinuteOnError)), DefaultValue(DEFAULT_CONTINUE_ON_ERROR), Browsable(true)]
        public virtual bool ContinuteOnError
        {
            get { return continueOnError; }
            set
            {
                if (continueOnError != value)
                {
                    continueOnError = value;
                    OnPropertyChanged(nameof(continueOnError));
                    RefreshName();
                }
            }
        }

        protected ProcessingMode processingMode = DEFAULT_PROCESSING_MODE;

        /// <summary>
        /// Gets or sets a value that determines whether operations are processed individually or in a batch.
        /// </summary>
        [GlobalisedCategory(typeof(Dynamics365RecordOperation), nameof(ProcessingMode)), GlobalisedDisplayName(typeof(Dynamics365RecordOperation), nameof(ProcessingMode)), GlobalisedDecription(typeof(Dynamics365RecordOperation), nameof(ProcessingMode)), DefaultValue(DEFAULT_PROCESSING_MODE), Browsable(true)]
        public ProcessingMode ProcessingMode
        {
            get { return processingMode; }
            set
            {
                if (processingMode != value)
                {
                    processingMode = value;
                    CoreUtility.SetBrowsable(this, nameof(BatchSize), processingMode == ProcessingMode.Batch);
                    OnPropertyChanged(nameof(ProcessingMode));
                    RefreshName();
                }
            }
        }

        #endregion

        /// <summary>
        /// Initialises a new instance of the Dynamics365RecordOperation class with the specified parent batch.
        /// </summary>
        /// <param name="parentBatch">The parent batch.</param>
        public Dynamics365RecordOperation(Batch parentBatch) : base(parentBatch)
        {
            SetTargetSource(DEFAULT_TARGET_SOURCE);
        }

        public override ValidationResult Validate()
        {
            ValidationResult result = base.Validate();

            result.AddErrorIf(Entity == default(Dynamics365Entity), Properties.Resources.Dynamics365RecordOperationValidateEntity, nameof(Entity));
            result.AddErrorIf(DataSource == default(IDataSource), Properties.Resources.Dynamics365RecordOperationValidateDataSource, nameof(DataSource));
            if (DataSource != default(IDataSource)) result.Errors.AddRange(DataSource.Validate().Errors);
            result.AddErrorIf(ProcessingMode == ProcessingMode.Batch && BatchSize <= 0, Properties.Resources.Dynamics365RecordOperationValidateBatchSize, nameof(BatchSize));

            if (CanValidateTarget())
            {
                result.AddErrorIf(TargetSource != TargetSource.None && Target == default(FieldValue), Properties.Resources.Dynamics365RecordOperationValidateTargetRecord, nameof(Target));

                if (Target != default(FieldValue))
                {
                    result.Errors.AddRange(Target.Validate().Errors);
                }
            }

            return result;
        }

        public virtual bool CanValidateTarget()
        {
            return true;
        }

        /// <summary>
        /// Executes operations against a Dynamics 365 organisation.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        public void Execute(CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            progress.Report(new ExecutionProgress(NotificationType.Information, string.Format(Properties.Resources.Dynamics365RecordOperationExecute, Name)));

            DataTable data = GetSourceData(cancel, progress);
            int batchSize = ProcessingMode == ProcessingMode.Individual ? 1 : BatchSize;
            List<OrganizationRequest> requests = new List<OrganizationRequest>();

            using (OrganizationServiceProxy service = Connection.OrganizationServiceProxy)
            {
                progress.Report(new ExecutionProgress(ExecutionStage.Transform, 0, data.Rows.Count));
                int batchIndex = 0;

                for (int rowIndex = 0; rowIndex < data.Rows.Count && !cancel.IsCancellationRequested; rowIndex++)
                {
                    try
                    {
                        requests.AddRange(CreateOrganisationRequests(data.Rows[rowIndex], cancel, progress));
                        progress.Report(new ExecutionProgress(ExecutionStage.Transform, rowIndex + 1, data.Rows.Count));

                        if (rowIndex >= ((batchIndex + 1) * batchSize) - 1 || rowIndex >= data.Rows.Count - 1)
                        {
                            if (ProcessingMode == ProcessingMode.Individual)
                            {
                                ExecuteIndividualRequests(requests, service, cancel, progress);
                            }
                            else if (ProcessingMode == ProcessingMode.Batch)
                            {
                                ExecuteBatchedRequests(requests, service, cancel, progress);
                            }

                            progress.Report(new ExecutionProgress(ExecutionStage.Load, rowIndex + 1, data.Rows.Count));
                            requests.Clear();
                            batchIndex++;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ContinuteOnError)
                        {
                            progress.Report(new ExecutionProgress(NotificationType.Warning, ex.Message));
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }

            if (!cancel.IsCancellationRequested)
            {
                progress.Report(new ExecutionProgress(NotificationType.Information, string.Format(Properties.Resources.Dynamics365RecordOperationExecuteSuccess, Name)));
            }
        }

        /// <summary>
        /// Executes the specified requests individually against Dynamics 365.
        /// </summary>
        /// <param name="requests">The requests to execute.</param>
        /// <param name="service">The organisation service.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        protected virtual void ExecuteIndividualRequests(List<OrganizationRequest> requests, IOrganizationService service, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            for (int requestIndex = 0; requestIndex < requests.Count && !cancel.IsCancellationRequested; requestIndex++)
            {
                try
                {
                    service.Execute(requests[requestIndex]);
                    progress.Report(new ExecutionProgress(NotificationType.Information, GetRequestDescription(requests[requestIndex])));
                }
                catch (Exception ex)
                {
                    if (ContinuteOnError)
                    {
                        progress.Report(new ExecutionProgress(NotificationType.Warning, ex.Message));
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Executes the specified requests in a batch against Dynamics 365.
        /// </summary>
        /// <param name="requests">The requests to execute.</param>
        /// <param name="service">The organisation service.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        protected void ExecuteBatchedRequests(List<OrganizationRequest> requests, IOrganizationService service, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = ContinuteOnError,
                    ReturnResponses = false
                },
                Requests = new OrganizationRequestCollection()
            };
            executeMultipleRequest.Requests.AddRange(requests.GetRange(0, requests.Count));

            ExecuteMultipleResponse response = (ExecuteMultipleResponse)service.Execute(executeMultipleRequest);

            if (response.IsFaulted)
            {
                List<ExecuteMultipleResponseItem> errors = response.Responses.Where(r => r.Fault != null).ToList();

                if (errors.Count > 0)
                {
                    OrganizationServiceFault error = errors.First().Fault;
                    string errorMessage = string.Format(Properties.Resources.Dynamics365RecordOperationExecuteBatchErrors, errors.Count, executeMultipleRequest.Requests.Count, error.Message);

                    if (ContinuteOnError)
                    {
                        progress.Report(new ExecutionProgress(NotificationType.Warning, errorMessage));
                    }
                    else
                    {
                        throw new ApplicationException(errorMessage);
                    }
                }
            }
            else
            {
                progress.Report(new ExecutionProgress(NotificationType.Information, string.Format(Properties.Resources.Dynamics365RecordOperationExecuteBatchSuccess, executeMultipleRequest.Requests.Count)));
            }
        }

        protected abstract string GetRequestDescription(OrganizationRequest request);

        protected abstract List<OrganizationRequest> CreateOrganisationRequests(DataRow row, CancellationToken cancel, IProgress<ExecutionProgress> progress);

        protected virtual Entity GetTargetEntity(DataRow row, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            Entity targetEntity = new Entity(Entity.LogicalName);

            if (TargetSource != TargetSource.None)
            {
                targetEntity.Id = (Guid)Target.GetValue(row, cancel, progress);
            }

            return targetEntity;
        }

        /// <summary>
        /// Gets data from the operation's data source.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The progress.</param>
        /// <returns></returns>
        protected virtual DataTable GetSourceData(CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            return DataSource?.GetDataTable(cancel, progress);
        }

        public override void UpdateParentReferences(Batch parentBatch)
        {
            base.UpdateParentReferences(parentBatch);
            Target?.UpdateParentReferences(this);
        }

        public override IOperation Clone(bool addSuffix)
        {
            Dynamics365RecordOperation clone = (Dynamics365RecordOperation)base.Clone(addSuffix);
            clone.Target = Target?.Clone();
            return clone;
        }

        #region Interface Methods

        public List<DataTableField> GetDataSourceFields()
        {
            List<DataTableField> fields = new List<DataTableField>();
            try
            {
                fields.AddRange(DataTableField.GetDataTableFields(DataSource?.GetDataColumns()));
            }
            catch { }

            return fields;
        }

        public LookupCriteria CreateLookupCriteria(Type type)
        {
            return (LookupCriteria)Activator.CreateInstance(type, new object[] { this, default(LookupValue) });
        }

        public virtual List<Field> GetDataDestinationFields()
        {
            List<Field> fields = new List<Field>();
            try
            {
                fields.AddRange(Entity.GetFields(Connection));
            }
            catch { }

            return fields;
        }

        public IConnection GetConnection()
        {
            return Connection;
        }

        public List<Dynamics365Field> GetDynamics365EntityFields()
        {
            List<Dynamics365Field> fields = new List<Dynamics365Field>();
            try
            {
                fields.AddRange(Dynamics365Field.GetFields(Entity, Connection));
            }
            catch { }

            return fields;
        }

        #endregion
    }

    /// <summary>
    /// Defines the types of supported target sources.
    /// </summary>
    public enum TargetSource
    {
        None,
        DataSourceValue,
        LookupValue,
        UserProvidedValue
    }

    /// <summary>
    /// Defines the types of supported processing modes.
    /// </summary>
    public enum ProcessingMode
    {
        Individual,
        Batch
    }
}
