﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    interface IDynamics365StatusesProvider
    {
        List<Dynamics365Status> GetStatuses();
    }
}
