﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    interface IDynamics365ViewsProvider
    {
        List<Dynamics365View> GetDynamics365Views();
    }
}
