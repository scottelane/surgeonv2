﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    public interface IDynamics365RelationshipsProvider
    {
        List<Dynamics365Relationship> GetRelationships();
    }
}
