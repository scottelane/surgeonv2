﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    public interface IDynamics365WorkflowProvider
    {
        List<Dynamics365Workflow> GetWorkflows();
    }
}
