﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    public interface IDynamics365SolutionsProvider
    {
        List<Dynamics365Solution> GetSolutions();
    }
}
