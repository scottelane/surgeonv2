﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    interface IDynamics365TeamsProvider
    {
        List<Dynamics365Team> GetTeams();
    }
}
