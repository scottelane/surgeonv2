﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    public interface IDynamics365UsersProvider
    {
        List<Dynamics365User> GetUsers();
    }
}
