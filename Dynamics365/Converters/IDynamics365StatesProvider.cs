﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    public interface IDynamics365StatesProvider
    {
        List<Dynamics365State> GetStates();
    }
}
