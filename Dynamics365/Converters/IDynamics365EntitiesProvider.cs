﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.Dynamics365
{
    public interface IDynamics365EntitiesProvider
    {
        List<Dynamics365Entity> GetEntities();
    }
}
