﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading;
using ScottLane.SurgeonV2.Core;
using Newtonsoft.Json;

namespace ScottLane.SurgeonV2.Dynamics365
{
    /// <summary>
    /// A LookupValue implementation that supports populating Dynamics 365 lookups.
    /// </summary>
    public class Dynamics365LookupValue : LookupValue, IDynamics365TargetEntitiesProvider
    {
        [GlobalisedDisplayName(typeof(Dynamics365LookupValue), nameof(DestinationField)), GlobalisedDecription(typeof(Dynamics365LookupValue), nameof(DestinationField)), TypeConverter(typeof(Dynamics365FieldConverter)), JsonProperty(Order = 1)]
        public override Field DestinationField
        {
            get { return base.DestinationField; }
            set
            {
                base.DestinationField = value;
                CoreUtility.SetBrowsable(this, nameof(TargetEntity), ((Dynamics365Field)DestinationField).Targets?.Length > 1);
            }
        }

        private Dynamics365Entity targetEntity;

        /// <summary>
        /// Gets or sets the entity that will be looked up if the destination field is a multi-target lookup.
        /// </summary>
        [GlobalisedCategory("General"), GlobalisedDisplayName("Target Entity"), GlobalisedDecription("The lookup target entity."), TypeConverter(typeof(Dynamics365TargetEntityConverter)), Browsable(false), JsonProperty(Order = 2)]
        public Dynamics365Entity TargetEntity
        {
            get { return targetEntity; }
            set
            {
                if (targetEntity != value)
                {
                    targetEntity = value;
                    OnPropertyChanged(nameof(TargetEntity));
                }
            }
        }

        /// <summary>
        /// Initialises a new instance of the Dynamics365LookupValue class with the specified parent operation.
        /// </summary>
        /// <param name="parentOperation">The parent operation.</param>
        public Dynamics365LookupValue(Operation parentOperation) : base(parentOperation)
        { }

        public override object GetValue(DataRow row, CancellationToken cancel, IProgress<ExecutionProgress> progress)
        {
            object value = base.GetValue(row, cancel, progress);
            return Dynamics365TypeConverter.Convert(value, ((Dynamics365RecordOperation)Parent).Entity, ((Dynamics365Field)DestinationField).LogicalName, ((Dynamics365RecordOperation)Parent).Connection, targetEntity?.LogicalName);
        }

        /// <summary>
        /// Overrides the ToString method.
        /// </summary>
        /// <returns>The destination field display name.</returns>
        public override string ToString()
        {
            return DestinationField?.DisplayName ?? string.Empty;
        }

        public List<Dynamics365Entity> GetTargetEntities()
        {
            List<Dynamics365Entity> targetEntities = new List<Dynamics365Entity>();

            try
            {
                // todo - this should only display on the form for fields that are lookups. it's not tied in any way to the dynamics365lookupvalue
                if (((Dynamics365Field)DestinationField).Targets != default(string[]))
                {
                    foreach (string target in ((Dynamics365Field)DestinationField).Targets)
                    {
                        targetEntities.Add(Dynamics365Entity.Create(target, ((Dynamics365RecordOperation)Parent).Connection));
                    }
                }
            }
            catch
            { }

            return targetEntities;
        }
    }
}