﻿using System.Collections.Generic;

namespace ScottLane.SurgeonV2.File
{
    public interface ISheetsProvider
    {
        List<string> GetSheets();
    }
}
