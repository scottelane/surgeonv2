﻿using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.File
{
    public class JsonSaveFileNameEditor : SaveFileNameEditor
    {
        public JsonSaveFileNameEditor()
        {
            filter = "JSON files (*.json)|*.json";
        }
    }
}
