﻿using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.File
{
    public class CsvSaveFileNameEditor : SaveFileNameEditor
    {
        public CsvSaveFileNameEditor()
        {
            filter = "CSV files (*.csv)|*.csv";
        }
    }
}
