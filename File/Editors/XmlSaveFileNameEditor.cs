﻿using ScottLane.SurgeonV2.Core;

namespace ScottLane.SurgeonV2.File
{
    public class XmlSaveFileNameEditor : SaveFileNameEditor
    {
        public XmlSaveFileNameEditor()
        {
            filter = "XML files (*.xml)|*.xml";
        }
    }
}
