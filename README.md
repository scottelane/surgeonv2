# Overview #

SurgeonV2 is a tool for [Dynamics 365 Customer Engagement](https://dynamics.microsoft.com/en-au/) that allows you to:

- Create, delete, update, upsert, activate, deactivate, set state, associate or deassociate Dynamics 365 records
- Run on-demand workflows against Dynamics 365 records
- Compare or find duplicate records in Dynamics 365, a CSV file or SQL Server
- Count the number of records in an entity, view or fetch-xml query
- Publish and export customisations
- Extract records from Dynamics 365 into a CSV, JSON or XML file
- Load records from SQL Server or a CSV file into Dynamics 365
- Migrate Dynamics 365 records from one environment to another
- Automate the tasks above using a command line interface

# Download #

Download [Installer](https://bitbucket.org/scottelane/surgeonv2/downloads/SurgeonV2%2020190209.msi) or [Zip](https://bitbucket.org/scottelane/surgeonv2/downloads/SurgeonV2%2020190209.zip) of the latest version (6 February 2019). See the [Downloads](https://bitbucket.org/scottelane/surgeonv2/downloads/) page for all downloads.

# Screenshot #

![SurgeonV2-2.png](https://bitbucket.org/repo/kpXadz/images/2424863693-SurgeonV2-2.png)

# Getting Started #

View the [Getting Started](https://bitbucket.org/scottelane/surgeonv2/wiki/Getting%20Started) guide over at the [wiki](https://bitbucket.org/scottelane/surgeonv2/wiki/).

# Acknowledgements #

The following people and products deserve recognition for their contribution to SurgeonV2:

* [Prasad Udawatte](https://www.linkedin.com/pub/prasad-udawatte/12/450/634) for passing on a lot of his technical knowledge to help get an early prototype of the original Surgeon tool off the ground
* [Live Charts](https://lvcharts.net/) for their excellent charting framework
* [Icons8](https://icons8.com/) for their wonderful icons I use on every Dynamics project
* [DockPanel Suite](https://github.com/dockpanelsuite/dockpanelsuite) for their nifty WinForms docking framework

Surgeon was developed and is maintained by [Scott Lane](http://au.linkedin.com/pub/scott-lane/31/218/b25), a Dynamics CRM consultant from Melbourne, Australia.
