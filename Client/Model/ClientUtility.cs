﻿using ScottLane.SurgeonV2.Client.Controls;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ScottLane.SurgeonV2.Client.Model
{
    /// <summary>
    /// Provides client helper methods.
    /// </summary>
    public class ClientUtility
    {
        public static BindableTreeNode GetContextMenuTreeviewNode(BindableTreeView treeView, object sender)
        {
            BindableTreeNode node = default(BindableTreeNode);

            if (sender is ContextMenuStrip)
            {
                ContextMenuStrip contextMenuStrip = (ContextMenuStrip)sender;
                TreeViewHitTestInfo testInfo = treeView.HitTest(treeView.PointToClient(new Point(contextMenuStrip.Left, contextMenuStrip.Top)));

                if (testInfo.Node != default(TreeNode))
                {
                    node = (BindableTreeNode)testInfo.Node;
                }
            }
            else if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
                node = GetContextMenuTreeviewNode(treeView, menuItem.Owner);
            }
            else if (sender is ToolStripDropDownMenu)
            {
                ToolStripDropDownMenu menu = (ToolStripDropDownMenu)sender;
                node = GetContextMenuTreeviewNode(treeView, menu.OwnerItem);
            }

            return node;
        }

        /// <summary>
        /// Gets a string representation of a timespan.
        /// </summary>
        /// <param name="timespan">The timespan.</param>
        /// <returns>The string representation.</returns>
        public static string GetDurationString(TimeSpan timespan)
        {
            string durationString = string.Empty;

            if (timespan.TotalHours >= 1)
            {
                int hours = Convert.ToInt32(Math.Round(timespan.TotalHours, 0));
                string hoursPlural = hours == 1 ? string.Empty : "s";
                int minutes = Convert.ToInt32(Math.Round(timespan.TotalMinutes % 60, 0));
                string minutesPlural = minutes == 1 ? string.Empty : "s";

                durationString = string.Format("{0} hour{1}, {2} minute{3}", hours, hoursPlural, minutes, minutesPlural);
            }
            else if (timespan.TotalMinutes >= 1)
            {
                int minutes = Convert.ToInt32(Math.Round(timespan.TotalMinutes, 0));
                string minutesPlural = minutes == 1 ? string.Empty : "s";
                int seconds = Convert.ToInt32(Math.Round(timespan.TotalSeconds % 60, 0));
                string secondsPlural = seconds == 1 ? string.Empty : "s";

                durationString = string.Format("{0} minute{1}, {2} second{3}", minutes, minutesPlural, seconds, secondsPlural);
            }
            else
            {
                int seconds = Convert.ToInt32(Math.Round(timespan.TotalSeconds, 0));
                string secondsPlural = seconds == 1 ? string.Empty : "s";

                durationString = string.Format("{0} second{1}", seconds, secondsPlural);
            }

            return durationString;
        }
    }
}