﻿using System;
using ScottLane.SurgeonV2.Client.Model;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;

namespace ScottLane.SurgeonV2.Client.Forms
{
    public partial class PerformanceForm : ChildForm
    {
        private readonly TimeSpan MINIMUM_UPDATE_DURATION = TimeSpan.FromSeconds(0.5);

        private DateTime processStartedOn;
        private TimeSpan processDuration;
        private DateTime extractLastUpdatedOn;
        private int lastLoadIndex;
        private DateTime loadLastUpdatedOn;

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }

        SeriesCollection series;
        LineSeries operationPerformance;

        public PerformanceForm()
        {
            InitializeComponent();
            ResetCounters();

            ApplicationState.Default.AsyncProcessStarted += Current_AsyncProcessStarted;
            ApplicationState.Default.AsyncProgressChanged += Current_AsyncProgressChanged;
        }

        private void ResetCounters()
        {
            processStartedOn = DateTime.MinValue;
            processDuration = TimeSpan.MinValue;
            extractLastUpdatedOn = DateTime.MinValue;
            lastLoadIndex = 0;
            loadLastUpdatedOn = DateTime.MinValue;
            performanceChart.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Load",
                    Values = new ChartValues<ObservablePoint>(),
                    PointGeometry = null,
                    LineSmoothness = 0
                }
            };

            performanceChart.LegendLocation = LegendLocation.None;

            series = new SeriesCollection();
            operationPerformance = new LineSeries()
            {
                Title = "Operations",
                Values = new ChartValues<ObservablePoint>()
            };
            series.Add(operationPerformance);
        }

        private void Current_AsyncProcessStarted(object sender, AsyncEventArgs e)
        {
            ResetCounters();
        }

        private void Current_AsyncProgressChanged(object sender, ProgressEventArgs e)
        {
            DateTime progressUpdatedOn = DateTime.Now;

            if (processStartedOn == DateTime.MinValue)
            {
                processStartedOn = progressUpdatedOn;
            }

            TimeSpan overallDuration = progressUpdatedOn - processStartedOn;

            if (overallDuration.TotalSeconds > 0)
            {
                if (e.Progress.ExecutionStage == Core.ExecutionStage.Load)
                {
                    int itemCount = e.Progress.ItemIndex - lastLoadIndex;
                    TimeSpan durationSinceLastProgress = progressUpdatedOn - (loadLastUpdatedOn == DateTime.MinValue ? processStartedOn : loadLastUpdatedOn);
                    double operationsPerSecond = itemCount / durationSinceLastProgress.TotalSeconds;
                    if (durationSinceLastProgress >= MINIMUM_UPDATE_DURATION) performanceChart.Series[0].Values.Add(new ObservablePoint(overallDuration.TotalSeconds, operationsPerSecond));
                    lastLoadIndex = e.Progress.ItemIndex;
                    loadLastUpdatedOn = progressUpdatedOn;
                }
            }
        }

        private void PerformanceForm_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            ApplicationState.Default.AsyncProgressChanged -= Current_AsyncProgressChanged;
            ApplicationState.Default.AsyncProcessStarted -= Current_AsyncProcessStarted;
        }
    }
}
