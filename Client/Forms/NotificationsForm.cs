﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ScottLane.SurgeonV2.Client.Model;
using Equin.ApplicationFramework;

namespace ScottLane.SurgeonV2.Client.Forms
{
    /// <summary>
    /// http://blw.sourceforge.net/
    /// </summary>
    public partial class NotificationsForm : ChildForm
    {
        readonly TimeSpan THROTTLE_INTERVAL = TimeSpan.FromSeconds(1);
        const int MAXIMUM_NOTIFICATIONS_PER_INTERVAL = 10;
        const int MAXIMUM_MESSAGES_TO_DISPLAY = 500;

        DateTime lastThrottledOn;
        int notificationsSinceLastInterval;
        BindingList<NotificationEventArgs> notifications;
        BindingListView<NotificationEventArgs> notificationsView;

        // https://msdn.microsoft.com/en-us/library/ms993236.aspx

        public NotificationsForm()
        {
            InitializeComponent();

            notifications = new BindingList<NotificationEventArgs>();
            notificationsView = new BindingListView<NotificationEventArgs>(notifications);
            notificationsView.ListChanged += Notifications_ListChanged;

            NotificationDataGridView.AutoGenerateColumns = false;
            NotificationDataGridView.Columns[1].DataPropertyName = "Message";
            NotificationDataGridView.Columns[2].DataPropertyName = "TimeStamp";
            NotificationDataGridView.DataSource = notificationsView;
            SortDataGridView(NotificationDataGridView.Columns[2]);

            ApplicationState.Default.NotificationRaised += Current_NotificationRaised;
            ApplicationState.Default.AsyncProgressChanged += Current_AsyncProgressChanged;

            UpdateNotificationFilter();
            UpdateFilterButtonText();
        }

        private void Notifications_ListChanged(object sender, ListChangedEventArgs e)
        {
            UpdateFilterButtonText();
        }

        private void UpdateFilterButtonText()
        {
            messagesToolStripButton.Text = string.Format(Properties.Resources.NotificationFormMessagesText, notifications.Count(n => n.NotificationType == Core.NotificationType.Information));
            warningsToolStripButton.Text = string.Format(Properties.Resources.NotificationFormWarningsText, notifications.Count(n => n.NotificationType == Core.NotificationType.Warning));
            errorsToolStripButton.Text = string.Format(Properties.Resources.NotificationFormErrorsText, notifications.Count(n => n.NotificationType == Core.NotificationType.Error));
        }

        private void Current_NotificationRaised(object sender, NotificationEventArgs e)
        {
            AddThrottledNotification(e);
        }

        private void Current_AsyncProgressChanged(object sender, ProgressEventArgs e)
        {
            if (e.Progress.ProgressType == Core.ProgressType.Notification)
            {
                AddThrottledNotification(new NotificationEventArgs(e.Progress.NotificationType, e.Progress.Message));
            }
        }

        private void AddThrottledNotification(NotificationEventArgs e)
        {
            if (lastThrottledOn == default(DateTime))
            {
                lastThrottledOn = DateTime.Now;
                notificationsSinceLastInterval = 0;
            }

            notificationsSinceLastInterval++;

            if (notificationsSinceLastInterval < MAXIMUM_NOTIFICATIONS_PER_INTERVAL)
            {
                notifications.Add(e);

                if (notifications.Count > MAXIMUM_MESSAGES_TO_DISPLAY)
                {
                    notifications.RemoveAt(0);
                }
            }

            if (DateTime.Now - lastThrottledOn > THROTTLE_INTERVAL)
            {
                lastThrottledOn = DateTime.Now;
                notificationsSinceLastInterval = 0;
            }
        }

        private void MessagesToolStripButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateNotificationFilter();
        }

        private void WarningsToolStripButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateNotificationFilter();
        }

        private void ErrorsToolStripButton_CheckedChanged(object sender, EventArgs e)
        {
            UpdateNotificationFilter();
        }

        private void UpdateNotificationFilter()
        {
            notificationsView.ApplyFilter(n => n.NotificationType == Core.NotificationType.Information && messagesToolStripButton.Checked ||
                                                n.NotificationType == Core.NotificationType.Warning && warningsToolStripButton.Checked ||
                                                n.NotificationType == Core.NotificationType.Error && errorsToolStripButton.Checked);
        }

        private void ClearAllToolStripButton_Click(object sender, EventArgs e)
        {
            notifications.Clear();
        }

        private void NotificationDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            NotificationEventArgs notification = ((ObjectView<NotificationEventArgs>)NotificationDataGridView.Rows[e.RowIndex].DataBoundItem).Object;

            if (e.ColumnIndex == 0)
            {
                if (notification.NotificationType == Core.NotificationType.Error)
                {
                    e.Value = Properties.Resources.NotificationsFormError;
                }
                else if (notification.NotificationType == Core.NotificationType.Information)
                {
                    e.Value = Properties.Resources.Information;
                }
                else if (notification.NotificationType == Core.NotificationType.Warning)
                {
                    e.Value = Properties.Resources.NotificationsFormWarning;
                }
            }
        }

        private void NotificationDataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            NotificationDataGridView.ClearSelection();
        }

        private void NotificationDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewColumn column = NotificationDataGridView.Columns[e.ColumnIndex];
            SortDataGridView(column);
        }

        private void SortDataGridView(DataGridViewColumn column)
        {
            if (column.SortMode == DataGridViewColumnSortMode.Automatic)
            {
                notificationsView.ApplySort(string.Format("{0} {1}", column.DataPropertyName, column.HeaderCell.SortGlyphDirection == SortOrder.Ascending ? "ASC" : "DESC"));
            }
        }

        private void NotificationDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NotificationEventArgs notification = ((ObjectView<NotificationEventArgs>)NotificationDataGridView.Rows[e.RowIndex].DataBoundItem).Object;
            ApplicationState.Default.SelectedError = notification;
        }

        private void NotificationDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView.HitTestInfo hitTest = NotificationDataGridView.HitTest(e.X, e.Y);
            }
        }

        private void NotificationDataGridView_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.C)
            {
                Clipboard.SetDataObject(NotificationDataGridView.GetClipboardContent());
            }
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(NotificationDataGridView.GetClipboardContent());
        }

        private void NotificationsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ApplicationState.Default.AsyncProgressChanged -= Current_AsyncProgressChanged;
            ApplicationState.Default.NotificationRaised -= Current_NotificationRaised;            
        }
    }
}
