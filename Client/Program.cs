﻿using System;
using System.IO;
using System.Windows.Forms;
using ScottLane.SurgeonV2.Client.Forms;
using ScottLane.SurgeonV2.Client.Model;

namespace ScottLane.SurgeonV2.Client
{
    static class Program
    {
        static MainForm mainForm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                if (Properties.Settings.Default.UpgradeSettings)
                {
                    // prevent settings from being reset when upgrading the application
                    Properties.Settings.Default.Upgrade();
                    Properties.Settings.Default.UpgradeSettings = false;
                    Properties.Settings.Default.Save();
                }

                CommandLineArguments arguments = CommandLineArguments.Parse(args);

                if (arguments.OpenProject)
                {
                    mainForm = new MainForm(arguments.ProjectPath);
                }
                else
                {
                    mainForm = new MainForm();
                }

                if (arguments.Execute)
                {
                    if (arguments.QuitAfterExecution)
                    {
                        ApplicationState.Default.AsyncProcessStopped += Default_AsyncProcessStopped;
                    }

                    if (arguments.ExecuteItemID != default(Guid))
                    {
                        ApplicationState.Default.RequestExecution(ApplicationState.Default.ActiveProject.FindExecutableItem(arguments.ExecuteItemID));
                    }
                    else
                    {
                        ApplicationState.Default.RequestExecution(ApplicationState.Default.ActiveProject);
                    }
                }

                ApplicationState.Default.NotificationRaised += Default_NotificationRaised;

                Application.Run(mainForm);
            }
            catch (Exception ex)
            {
                LogToFile(DateTime.Now, ex.Message, ex.StackTrace);
            }

            return Environment.ExitCode;
        }

        private static void Default_NotificationRaised(object sender, NotificationEventArgs e)
        {
            if (e.NotificationType == Core.NotificationType.Error)
            {
                LogToFile(e.TimeStamp, e.Message, e?.Exception?.StackTrace);
            }
        }

        private static void LogToFile(DateTime timeStamp, string message, string stackTrace)
        {
            try
            {
                using (StreamWriter writer = System.IO.File.AppendText("ErrorLog.txt"))
                {
                    writer.WriteLine("{0} {1} {2}", timeStamp, message ?? string.Empty , stackTrace ?? string.Empty);
                }
            }
            catch { }
        }

        private static void Default_AsyncProcessStopped(object sender, AsyncStoppedEventArgs e)
        {
            if (e.CompletedSuccessfully)
            {
                mainForm.Close();
            }
            else
            {
                ApplicationState.Default.AsyncProcessStopped -= Default_AsyncProcessStopped;
            }
        }
    }
}
