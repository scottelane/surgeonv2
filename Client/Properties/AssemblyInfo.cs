﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SurgeonV2")]
[assembly: AssemblyDescription("SurgeonV2 is a tool for Dynamics 365 Customer Engagement that allows records to be easily created, deleted, updated, counted, compared and migrated between environments. It also allows records to be imported from SQL Server, a Microsoft Excel worksheet or a CSV file, and can export records to a CSV, XML or JSON file. SurgeonV2 can also publish and export customisations and has full command-line support.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Scott Lane")]
[assembly: AssemblyProduct("SurgeonV2")]
[assembly: AssemblyCopyright("Copyright © 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5bb55063-17ef-4739-a08b-1a3362a22428")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the "*" as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.*")]
//[assembly: AssemblyFileVersion("1.0.0.0")]
